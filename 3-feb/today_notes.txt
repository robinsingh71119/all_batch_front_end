---Img Tag
The HTML <img> tag is used to embed an image in a web page.

The <img> tag has two required attributes:
 src - Specifies the path to the image
 alt - Specifies an alternate text for the image
 width and height attributes can also be used.


